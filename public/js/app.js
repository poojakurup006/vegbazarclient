angular.module("underscore", []).factory("_", function() {
    return window._
}),

angular.module("vegetablebazarapp", ["ng-sweet-alert","ionic" ,"vegetablebazarapp.controllers", "vegetablebazarapp.directives", "vegetablebazarapp.filters", "vegetablebazarapp.services", "vegetablebazarapp.factories", "vegetablebazarapp.config", "underscore","ngDialog"])

.run(function(CartService,$rootScope,$timeout,$state,$http) 
{

	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
    console.log(JSON.stringify(toState));
    
  $rootScope.disableCity=$rootScope.disableCity||false;
  $rootScope.search_data=$rootScope.search_data||'';
  localStorage.hideCart=localStorage.hideCart||true;
    $rootScope.globals = localStorage.globals|| {currentUser:''};

     console.log($rootScope.globals);
     $rootScope.globals = $rootScope.globals === localStorage.globals? angular.fromJson(localStorage.globals): {currentUser:''};

        if(! $rootScope.globals.hasOwnProperty('currentUser')){ 
       Object.defineProperty( $rootScope.globals,'currentUser',{value:''});}
        console.log('ttt'+ $rootScope.globals.hasOwnProperty('currentUser'));

        if ($rootScope.globals.currentUser!=='' ) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }
    var restrictedPage = $.inArray(toState.name, ['shipping','thankyou','profile']) !== -1;
            var loggedIn = $rootScope.globals.currentUser!==''? $rootScope.globals.currentUser: false;
            if (restrictedPage && !loggedIn) {
               event.preventDefault();
               console.log(JSON.stringify(restrictedPage));
               $state.go('signin');
            }

    var removePageCart= $.inArray(toState.name, ['shipping','thankyou','signin','otp','forgot-password','profile','contact']) !== -1;
    if (removePageCart) {
      localStorage.hideCart=false;

    }
    else
    {
      localStorage.hideCart=true;
    }


    });

  $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams){
    if(toState.name.indexOf('app.fooditems.vegetableslist') > -1)
    {
      // Restore platform default transition. We are just hardcoding android transitions to auth views.
     }
  });
})


.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  //INTRO
  .state('auth', {
    url: "/auth",
    views:{
      '':{
        templateUrl: "templates/auth/auth.html",
        controller: 'AuthCtrl'        
      },
      'changeHeader@auth':{
        templateUrl: "templates/header/firstHeader/changeHeader.html",
        controller: 'ChangeHeaderCtrl'
      },
      'header@auth':{
        templateUrl: "templates/header/secondHeader/header.html",
        controller:'HeaderCtrl'
      },
      'footer@auth':{
        templateUrl: "templates/footer/footer.html"
      }
    }
  })

  .state('vegetables', {
    url: "/vegetables",
    views:{
      '':{
        templateUrl: "templates/vegetables/veg.html",
        controller: 'vegetablesListCtrl'        
      },
      'changeHeader@vegetables':{
        templateUrl: "templates/header/firstHeader/changeHeader.html",
        controller: 'ChangeHeaderCtrl'
      },
      'header@vegetables':{
        templateUrl: "templates/header/secondHeader/header.html",
        controller:'HeaderCtrl'
      },
      'pagetitle@vegetables':{
        templateUrl: "templates/header/PageTitle/PageTitle.html"
      },
      'footer@vegetables':{
        templateUrl: "templates/footer/footer.html"
      }
    }
  })

  .state('fruits', {
    url: "/fruits",
    views:{
      '':{
        templateUrl: "templates/fruits/fruit.html",
        controller: 'fruitsListCtrl'        
      },
      'changeHeader@fruits':{
        templateUrl: "templates/header/firstHeader/changeHeader.html",
        controller: 'ChangeHeaderCtrl'
      },
      'header@fruits':{
        templateUrl: "templates/header/secondHeader/header.html",
        controller:'HeaderCtrl'
      },
      'pagetitle@fruits':{
        templateUrl: "templates/header/PageTitle/PageTitle.html"
      },
      'footer@fruits':{
        templateUrl: "templates/footer/footer.html"
      }
    }
  })

  .state('recharge', {
    url: "/recharge",
    views:{
      '':{
        templateUrl: "templates/rechrge/rechrge.html",
        controller: 'RechrgeCtrl'        
      },
      'changeHeader@recharge':{
        templateUrl: "templates/header/firstHeader/changeHeader.html",
        controller: 'ChangeHeaderCtrl'
      },
      'header@recharge':{
        templateUrl: "templates/header/secondHeader/header.html",
        controller:'HeaderCtrl'
      },
      'pagetitle@recharge':{
        templateUrl: "templates/header/PageTitle/PageTitle.html"
      },
      'footer@recharge':{
        templateUrl: "templates/footer/footer.html"
      }
    }
  })

  .state('signin', {
    url: "/signin",
    views:{
      '':{
        templateUrl: "templates/authentication/signin.html",
        controller: 'SignInCtrl'        
      },
      'registration@signin':{
        templateUrl: "templates/authentication/registration/registration.html",
        controller: 'SignupCtrl',
        resolve: {
            States_data: ['AuthService',function(AuthService) {
                     return AuthService.getStates();
            }],
            Master_City_data: ['AuthService',function(AuthService) {
                     return AuthService.getMasterCities();
            }],
            Cities_data: ['AuthService',function(AuthService) {
                     return AuthService.getCities();
            }],
            Areas_data: ['AuthService',function(AuthService) {
                     return AuthService.getAreas();
            }],
            Status_data: ['AuthService',function(AuthService) {
                     return AuthService.getStatus();
            }]


          }
      },
      'login@signin':{
        templateUrl: "templates/authentication/login/login.html",
        controller: 'LoginCtrl'
      },
      'changeHeader@signin':{
        templateUrl: "templates/header/firstHeader/changeHeader.html",
        controller: 'ChangeHeaderCtrl'
      },
      'header@signin':{
        templateUrl: "templates/header/secondHeader/header.html",
        controller:'HeaderCtrl'
      },
      'pagetitle@signin':{
        templateUrl: "templates/header/PageTitle/PageTitle.html"
      },
      'footer@signin':{
        templateUrl: "templates/footer/footer.html"
      }
    }
  })

  .state("otp", {
        url: "/otp/:mobile",
        views: {
            '': {
                templateUrl: "templates/otp/otp.html",
                controller: "oneTimePasswordCtrl"
              },
            'changeHeader@otp':{
              templateUrl: "templates/header/firstHeader/changeHeader.html",
              controller: 'ChangeHeaderCtrl'
            },
            'header@otp':{
              templateUrl: "templates/header/secondHeader/header.html",
              controller:'HeaderCtrl'
            },
            'footer@otp':{
              templateUrl: "templates/footer/footer.html"
            }
     }
  })

  .state("orders", {
        url: "/orders",
        views: {
            '': {
                templateUrl: "templates/orders/ordersController.html"
              },
            'changeHeader@orders':{
              templateUrl: "templates/header/firstHeader/changeHeader.html",
              controller: 'ChangeHeaderCtrl'
            },
            'header@orders':{
              templateUrl: "templates/header/secondHeader/header.html",
              controller:'HeaderCtrl'
            },
            'currentorder@orders':{
              templateUrl: "templates/orders/currentOrders/currentOrders.html",
              controller:'currentOrdersListCtrl'
            },
            'orderhistory@orders':{
              templateUrl: "templates/orders/ordersHistory/ordersHistory.html",
              controller:'ordersHistoryListCtrl'
            },
            'footer@orders':{
              templateUrl: "templates/footer/footer.html"
            }
     }
  })

  .state("orderDetails", {
        url: "/orders/orderDetails/:orderType/:id",
        views: {
            '': {
                templateUrl: "templates/orders/orderDetails/orderDetails.html",
                controller: "orderDetailsCtrl"
              },
            'changeHeader@orderDetails':{
              templateUrl: "templates/header/firstHeader/changeHeader.html",
              controller: 'ChangeHeaderCtrl'
            },
            'header@orderDetails':{
              templateUrl: "templates/header/secondHeader/header.html",
              controller:'HeaderCtrl'
            },
            'footer@orderDetails':{
              templateUrl: "templates/footer/footer.html"
            }
     }
  })

  .state("forgot-password", {
        url: "/forgot-password",
        views: {
            '': {
                templateUrl: "templates/forgot-password/forgot-password.html",
                controller: 'ForgotPasswordCtrl'
              },
            'changeHeader@forgot-password':{
              templateUrl: "templates/header/firstHeader/changeHeader.html",
              controller: 'ChangeHeaderCtrl'
            },
            'header@forgot-password':{
              templateUrl: "templates/header/secondHeader/header.html",
              controller:'HeaderCtrl'
            },
            'footer@forgot-password':{
              templateUrl: "templates/footer/footer.html"
            }
     }
  })

  .state('shipping', {
    url: "/shipping",
    views: {
      '': {
        templateUrl: "templates/shippingaddress/shippingaddress.html",
        controller: 'ShippingCtrl'
      },
      'changeHeader@shipping':{
        templateUrl: "templates/header/firstHeader/changeHeader.html",
        controller: 'ChangeHeaderCtrl'
      },
      'header@shipping':{
        templateUrl: "templates/header/secondHeader/header.html",
        controller:'HeaderCtrl'
      },
      'footer@shipping':{
        templateUrl: "templates/footer/footer.html"
      }
    },
    resolve: {
      address_data: ['AuthService', '$ionicLoading',function(AuthService, $ionicLoading) {
      return AuthService.getAddressData();
      }],
      time_data:['CartService',function(CartService){
         return CartService.getDeliveryTime();
       }],
       States_data: ['AuthService',function(AuthService) {
               return AuthService.getStates();
      }],
      Cities_data: ['AuthService',function(AuthService) {
               return AuthService.getCities();
      }],
      Areas_data: ['AuthService',function(AuthService) {
               return AuthService.getAreas();
      }]
    }
  })

  .state('blog', {
    url: "/blog",
    views:{
      '':{
        templateUrl: "templates/blog/blog.html",
        controller: 'BlogCtrl'        
      },
      'changeHeader@blog':{
        templateUrl: "templates/header/firstHeader/changeHeader.html",
        controller: 'ChangeHeaderCtrl'
      },
      'header@blog':{
        templateUrl: "templates/header/secondHeader/header.html",
        controller:'HeaderCtrl'
      },
      'pagetitle@blog':{
        templateUrl: "templates/header/PageTitle/PageTitle.html"
      },
      'footer@blog':{
        templateUrl: "templates/footer/footer.html"
      }
    }
  })

  .state('aboutus', {
    url: "/aboutus",
    views:{
      '':{
        templateUrl: "templates/aboutus/aboutus.html",
        controller: 'aboutusCtrl'        
      },
      'changeHeader@aboutus':{
        templateUrl: "templates/header/firstHeader/changeHeader.html",
        controller: 'ChangeHeaderCtrl'
      },
      'header@aboutus':{
        templateUrl: "templates/header/secondHeader/header.html",
        controller:'HeaderCtrl'
      },
      'pagetitle@aboutus':{
        templateUrl: "templates/header/PageTitle/PageTitle.html"
      },
      'footer@aboutus':{
        templateUrl: "templates/footer/footer.html"
      }
    }
  })

.state('contact', {
    url: "/contact",
    views:{
      '':{
        templateUrl: "templates/contact/contact.html",
        controller: 'ContactCtrl'        
      },
      'changeHeader@contact':{
        templateUrl: "templates/header/firstHeader/changeHeader.html",
        controller: 'ChangeHeaderCtrl'
      },
      'header@contact':{
        templateUrl: "templates/header/secondHeader/header.html",
        controller:'HeaderCtrl'
      },
      'pagetitle@contact':{
        templateUrl: "templates/header/PageTitle/PageTitle.html"
      },
      'footer@contact':{
        templateUrl: "templates/footer/footer.html"
      }
    }
  })

.state("logout", {
        url: "/logout",
        controller: "LogoutCtrl"
      })

.state("thankyou", {
        url: "/thankyou", 
        views: {
            '': {
                templateUrl: "templates/thankyou/thankyou.html",
                controller: "ThankyouCtrl"
              },
            'changeHeader@thankyou':{
              templateUrl: "templates/header/firstHeader/changeHeader.html",
              controller: 'ChangeHeaderCtrl'
            },
            'header@thankyou':{
              templateUrl: "templates/header/secondHeader/header.html",
              controller:'HeaderCtrl'
            },
            'footer@thankyou':{
              templateUrl: "templates/footer/footer.html"
            }
     }
  })

  .state("profile", {
        url: "/profile",
        views: {
            '': {
                templateUrl: "templates/profile/profile.html",
                controller: "profileCtrl"
              },
            'changeHeader@profile':{
              templateUrl: "templates/header/firstHeader/changeHeader.html",
              controller: 'ChangeHeaderCtrl'
            },
            'header@profile':{
              templateUrl: "templates/header/secondHeader/header.html",
              controller:'HeaderCtrl'
            },
            'footer@profile':{
              templateUrl: "templates/footer/footer.html"
            }
     },resolve: {
      address_data: ['AuthService', '$ionicLoading',function(AuthService, $ionicLoading) {
      return AuthService.getAddressData();
      }],
      States_data: ['AuthService',function(AuthService) {
               return AuthService.getStates();
      }],
      Master_City_data: ['AuthService',function(AuthService) {
               return AuthService.getMasterCities();
      }],
      Cities_data: ['AuthService',function(AuthService) {
               return AuthService.getCities();
      }],
      Areas_data: ['AuthService',function(AuthService) {
               return AuthService.getAreas();
      }],
       Status_data: ['AuthService',function(AuthService) {
               return AuthService.getStatus();
      }]

    }
  })


;


  $urlRouterProvider.otherwise('/auth');
});



angular.module('vegetablebazarapp.controllers').controller('AuthCtrl',[ '$scope','$timeout',function($scope,$timeout) {
	var slidesInSlideshow = 5;
	 var slidesTimeIntervalInMs = 5000; 

	 $scope.slideshow = 1;
	 var slideTimer =5000;
	 $timeout(function interval() {
	     $scope.slideshow = ($scope.slideshow % slidesInSlideshow) + 1;
	     slideTimer = $timeout(interval, slidesTimeIntervalInMs);
	     }, slidesTimeIntervalInMs);

}]);
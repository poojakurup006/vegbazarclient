angular.module('vegetablebazarapp.controllers').controller('ordersHistoryListCtrl', function($scope,$rootScope,OtherService,$ionicPopup) {

if($rootScope.orderHistory!== undefined && $rootScope.orderHistory!==''){ $scope.orders=$rootScope.orderHistory;}
else{

		OtherService.getOrdersHistory(angular.toJson($rootScope.globals.currentUser.username))
			.then(function(res){
				$scope.orders=res;
				$rootScope.orderHistory=res;

			}, function(err) { 
				
				$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});
}

})
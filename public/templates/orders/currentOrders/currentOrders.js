angular.module('vegetablebazarapp.controllers').controller('currentOrdersListCtrl', function($scope,$rootScope,OtherService,$ionicPopup,$http) {

  if($rootScope.currentOrders!== undefined && $rootScope.currentOrders!==''){  $scope.orders=$rootScope.currentOrders;}
else{
		OtherService.getCurrentOrders(angular.toJson($rootScope.globals.currentUser.username))
			.then(function(res){
				$scope.orders=res;
				$rootScope.currentOrders=res;

			}, function(err) { 
				
				$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});
	}
});
angular.module('vegetablebazarapp.controllers').controller('oneTimePasswordCtrl', function($scope, $state,$ionicLoading,AuthService,$ionicPopup,$stateParams) {
	//$scope.otp;
		$scope.mobile = $stateParams.mobile;
	$scope.resendStatus=false;
	$scope.submitOTP = function(testOTP){
		
        console.log(angular.toJson(testOTP  + "  " + $scope.mobile));
		AuthService.submitOTP(testOTP,$scope.mobile)
			.then(function(data){
				
				
				if(data.length>0 && data[0].success){$ionicPopup.alert({title: 'Success',
			     template:""+ data[0].message });
					if(data[0].success) $state.go('signin');
				}
				else if(data.length>0 && data[0].expired){
					$ionicPopup.alert({title: 'Success',template:""+ data[0].message });
					$scope.resendStatus=true;

				}
					else{$ionicPopup.alert({
			     title: 'Error',
			     template:"The OTP is not matching.. Please try again " });
				$scope.resendStatus=true;}
				},
				function(err) { 
				
				$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});

	};
		
	$scope.resendOTP = function(){
		
		AuthService.resendOTP(angular.toJson($scope.mobile))
			.then(function(data){
				$scope.resendStatus=false;
				$ionicPopup.alert({
			     title: 'Success',
			     template:"New OTP has been sent to your mobile " });
				$scope.otp='';
		
		
				},
				function(err) { 
				
				$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});
		
		};

	$scope.user = {};
});
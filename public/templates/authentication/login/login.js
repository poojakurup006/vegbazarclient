angular.module('vegetablebazarapp.controllers').controller('LoginCtrl', function($scope, $state, $templateCache, $q, $rootScope,$ionicPopup,AuthService,CartService) {
$rootScope.disableCity=false;
	$scope.doLogIn = function(){
		//$state.go('app.fooditems');
(function initController() {
            // reset login status
           
            AuthService.ClearCredentials();
            console.log("after clear credentials");
        })();

		/*$ionicLoading.show({
			template: 'logging in ...'
		});*/
		AuthService.doLogin(angular.toJson($scope.user))
               
			.then(function(response){

				 if (response.success) {

                	 //$ionicLoading.hide();
                	 if(response.isVerified==1){
                    AuthService.SetCredentials($scope.user.mobile, $scope.user.password);

                    	$scope.defaultCity=localStorage.city;
						$scope.loginCity=localStorage.loginCity;
                    
                    	localStorage.city=$scope.loginCity;
    					$rootScope.city=localStorage.city;
    					CartService.CartChange().then(function(res){console.log(res);
    						$rootScope.$broadcast('loginChange', { message: "test" });
            
        				},function(err){console.log("there is an error"+err);});
    					

    					$rootScope.disableCity=true;


                   
					//$state.go('app.fooditems.vegetableslist');
					if ($rootScope.sumofAll>=100) {
						$state.go('shipping');
					}
					else
					{
						//alert(angular.toJson(window.localStorage.city));
						$state.go('auth');
					}
                   }
                   else{
                   	$state.go('otp',{mobile:$scope.user.mobile});
                   }

                } else {
                	
                    console.log("erooooooooooooooor "+response.message);
                    $scope.user={};
                    $ionicPopup.alert({
			     title: 'Error',
			     template: response.message ? response.message:"There is an error.. Please try again " });
                }
				
			}, function(err) { 
				console.log(err);
				//$ionicLoading.hide();
				$ionicPopup.alert({
			     title: 'Error',
			     template: err ? angular.toJson(err):"There is an error.. Please try again " }
			     );
		});
		
		
	};

	$scope.user = {};

});
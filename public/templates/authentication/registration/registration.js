angular.module('vegetablebazarapp.controllers').controller('SignupCtrl', function($scope, $state,$ionicPopup, ngDialog, States_data, Master_City_data, Cities_data,Areas_data,Status_data, $timeout, AuthService) {
	
$scope.user = {};

	  
	      $scope.states =  States_data  || [
	        { id: 1, name: 'Maharashtra' },
	        { id: 2, name: 'MP' },
	        { id: 3, name: 'Gujarat' },
	        { id: 4, name: 'UP' }
	      ];	   	  
	   	  $scope.masterCity =  Master_City_data  || [
	        { id: 1, name: 'Uttrakhand' }
	      ];
	      $scope.citys = Cities_data  || [
	        { id: 1, name: 'Pune' },
	        { id: 2, name: 'Mumbai' },
	        { id: 3, name: 'Nagpur' },
	        { id: 4, name: 'Nasik' }
	      ];
	      $scope.Areas = Areas_data  || [
	        { id: 1, name: 'Area 1' },
	        { id: 2, name: 'Area 2' },
	        { id: 3, name: 'Area 3' },
	        { id: 4, name: 'Area 4' }
	      ];
	      $scope.Status = Status_data  || [
	        { id: 1, name: 'test1' },
	        { id: 2, name: 'Housewife' },
	        { id: 3, name: 'Working' },
	        { id: 4, name: 'Commercial' }
	      ];

 $scope.GetSelectedMasterCity=function(state){$scope.masterCity =  _.where( Master_City_data, { state_id:parseInt(state)} );};
 $scope.GetSelectedCities=function(mastercity){$scope.citys =  _.where( Cities_data, { master_city_id:parseInt(mastercity)} );};
 $scope.GetSelectedAreas=function(city){ $scope.Areas =  _.where( Areas_data, { city_id:parseInt(city)} );};
	    

	$scope.doSignUp = function(){
		delete $scope.user.confirmpassword;
		if($scope.user.email==='')  $scope.user.email="customer@vegetablebazar.com";  

		AuthService.doSignUp(angular.toJson($scope.user))
			.then(function(data){
				 if(data.insertId && data.insertId!==0){
					$state.go('otp',{mobile:$scope.user.mobile});					
				}
				else  if(data.indexOf("Mobile Already Exists")!== -1){$ionicPopup.alert({
			     title: 'Error',
			     template:" " + data});
				}
			}, function(err) { 				
				$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});
		
		
	};

	$scope.popup=function(){

		ngDialog.open({ template: 'templates/terms/terms.html', className: 'ngdialog-theme-default' });
	};
});
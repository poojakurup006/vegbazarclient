angular.module('vegetablebazarapp.controllers').controller('ForgotPasswordCtrl', function($scope, $state,$q,$ionicPopup,AuthService) {
	$scope.recoverPasswordonEmail = function(){

		AuthService.recoverPasswordonEmail(angular.toJson($scope.user.email))
			.then(function(res){
				if (res && res.length>0 ) {
				$ionicPopup.alert({
			     title: 'Success',
			     template:"Password has been sent to your Email " });
		          
		          $state.go('signin');
				}
				else{$ionicPopup.alert({
			     title: 'Error',
			     template:"Email does not exists in our records" });}
				 
			}, function(err) { 
				
				$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});
		
	};
	$scope.recoverPasswordonMobile = function(){

		AuthService.recoverPasswordonMobile(angular.toJson($scope.user.mobile))
			.then(function(res){
				if (res && res.length>0 ) {
				$ionicPopup.alert({
			     title: 'Success',
			     template:"Password has been sent to your Mobile " });
		          
		          $state.go('signin');
				}
				else{$ionicPopup.alert({
			     title: 'Error',
			     template:"Mobile does not exists in our records" });}
				 
			}, function(err) { 
				
				$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});
	};

	$scope.user = {};
});
angular.module('vegetablebazarapp.controllers').controller("ThankyouCtrl", ["$scope", "$rootScope", "$ionicLoading","_", function($scope,$rootScope, $ionicLoading, _) {
   	$ionicLoading.hide();
   	$scope.delivery_time=angular.fromJson($rootScope.delivery_time);
	$scope.total=$rootScope.total;
	localStorage.removeItem('oldTotal');
	localStorage.removeItem('offerdetails');
	localStorage.removeItem('discont');
	
}]);
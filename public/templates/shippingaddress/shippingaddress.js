angular.module('vegetablebazarapp.controllers').controller("ShippingCtrl", ["$scope","$state","$rootScope","address_data", "time_data","States_data","Cities_data","Areas_data","$filter","$ionicPopup","$ionicLoading","CartService", "_", function($scope, $state,$rootScope, address_data,time_data,States_data,Cities_data,Areas_data, $filter,$ionicPopup,$ionicLoading,CartService, _) {

    $scope.user = address_data;
    $scope.newaddress=false;
    $scope.isDisabled = false;
    //$scope.alltimings=time_data;

    $scope.alltimings =  _.where( time_data, { id:parseInt($scope.user.area)} );
         $scope.states =  States_data  || [
	        { id: 1, name: 'Maharashtra' },
	        { id: 2, name: 'MP' },
	        { id: 3, name: 'Gujarat' },
	        { id: 4, name: 'UP' }
	      ];
	   	  
	      $scope.citys = Cities_data  || [
	        { id: 1, name: 'Pune' },
	        { id: 2, name: 'Mumbai' },
	        { id: 3, name: 'Nagpur' },
	        { id: 4, name: 'Nasik' }
	      ];
	      $scope.Areas = Areas_data  || [
	        { id: 1, name: 'Area 1' },
	        { id: 2, name: 'Area 2' },
	        { id: 3, name: 'Area 3' },
	        { id: 4, name: 'Area 4' }
	      ];
	       $scope.delivery_time = time_data  || [
	        { id: 1, name: 'Area 1' },
	        { id: 2, name: 'Area 2' },
	        { id: 3, name: 'Area 3' },
	        { id: 4, name: 'Area 4' }
	      ];

	      $scope.GetSelectedArea=function(area){

	      	$scope.alltimings =  _.where( time_data, { id:parseInt(area)} );
		         
		    
	      };
	      $scope.GetSelectedArea($scope.user.area);

			Array.prototype.remove = function(value) {
			       this.splice(this.indexOf(value), 1);
			       return true;
			};

			if ($scope.alltimings.length === 0) {
				$scope.alltimings.push({"id":10,"name":"5:30AM to 10:30AM"});
			}
			var availabledelivertoday=0;
			angular.forEach($scope.alltimings, function(d){
				if (d.name == "Deliver Today") {
					availabledelivertoday=1;

				}			
			});

			if (availabledelivertoday === 0) {
				$scope.alltimings.push({"id":11,"name":"Deliver Today"});
			}
			
			$scope.time = '3:00 PM';
			var now = new Date();
			var nowTime = new Date((now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + now.getHours()+":"+now.getMinutes());
			var userTime = new Date((now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + $scope.time);
			if ((nowTime.getTime() > userTime.getTime()) || (nowTime.getTime() == userTime.getTime())) {
			       $scope.alltimings.remove("Deliver Today");
			}
    
     $scope.removeProductFromCart = function(test) {
        $ionicActionSheet.show({
            destructiveText: "Remove from cart",
            cancelText: "Cancel",
            cancel: function() {
                return !0;
            },
            destructiveButtonClicked: function() {
                return CartService.removeCartItem(test), $scope.products = CartService.getCartItems(), !0;
            }
        });};

      
   
     $scope.go = function() {
     	$scope.isDisabled = true;
     	$ionicLoading.show({
			template: 'Please Wait...'
		});
          	console.log("all order data" + JSON.stringify(CartService.getCartItems())); 
          	$scope.user.deliveryTime=angular.toJson($scope.user.deliveryTime.name);
          	delete $scope.user.onesignal_token;
          	delete $scope.user.contacts;
	  	CartService.ConfirmOrder(CartService.getCartItems(),$scope.user	).then(function(res){
		var total=$rootScope.sumofAll;
		$rootScope.delivery_time=$scope.user.deliveryTime;
     	$rootScope.total=$rootScope.sumofAll;
		$rootScope.sumofAll=0;
		localStorage.removeItem('lsallCartItems');
		localStorage.removeItem('offerdetails');
		localStorage.removeItem('discont');
		$rootScope.currentOrders='';

		$state.go('thankyou');

		},function(error){ console.log("error "+ error);});
     
     };
}]);